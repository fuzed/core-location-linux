// swift-tools-version:4.2

import PackageDescription

let package = Package(
    name: "CoreLocation-Linux",
    products: [
        .library(name: "CoreLocation-Linux",
                 targets: ["CoreLocation-Linux"]),
    ],
    targets: [
        .target(name: "CoreLocation-Linux", dependencies: [])
    ]
)
